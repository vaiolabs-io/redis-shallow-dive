# Redis Shallow Dive



.footer: Created By Alex M. Schapelle, VAioLabs.io


---

# About The Course Itself ?

We'll learn several topics mainly focused on:

- What is Redis ?
- Who needs Redis ?
- How Redis works ?
- How to manage Redis in various scenarios ?


---
# Who is this course for ?

- The name kind of mentions it:
    - System administrators who wish to learn basic Redis usage.
    - SysOps who are moving to DevOps jobs.
    - DevOps who wish to NoSQL with simple engine.
- But it also can be useful for:
    - Junior DevOps who wish to gain minimal knowledge of NoSQL.
    - Junior software developers who have no knowledge of NoSQL.

---

# Course Topics

- [Basics](../01_basics/README.md)
- [Hashes](../02_hashes/README.md)
- [Lists and Sets](../03_lists_and_sets/README.md)
- [Sorted Sets](../04_sorted_sets/README.md)
- [Key Naming Strategies](../05_key_naming/README.md)
- [Beyond Data Storage](../06_beyond_data_storage/README.md)
- [Watch Keys For Updates](../07_watch_keys_for_updates/README.md)

---
# About Me

<img src="../99_misc/.img/me.jpg" alt="me drawing" style="float:right;width:180px;">
<img src="../99_misc/.img/duffy.jpeg" alt="duffy drawing" style="float:right;width:180px;">

- Over 12 years of IT industry Experience.
- Fell in love with AS-400 unix system at IDF.
- 5 times tried to finish degree in computer science field
    - Between each semester, I tried to take IT course at various places.
        -  CompTIA A+.
        -  Cisco CCNA.
        -  RedHat RHCSA.
        -  LPIC1 and Shell scripting.
        -  Other stuff I've learned alone.

---

# About Me (cont.)

- Over 7 years of sysadmin:
    - Shell scripting fanatic
    - Python developer
    - JS admirer
    - Golang fallen
    - Rust fan
- 5 years of working with devops
    - Git supporter
    - Vagrant enthusiast
    - Ansible consultant
    - Container believer
    - K8s user

---
# About Me (cont.)

You can find me on the internet in bunch of places:

- Linkedin: [Alex M. Schapelle](https://www.linkedin.com/in/alex-schapelle)
- Gitlab: [Silent-Mobius](https://gitlab.com/silent-mobius)
- Github: [Zero-Pytagoras](https://github.com/zero-pytagoras)
- ASchapelle: [My Site](https://aschapelle.com)
- VaioLabs-IO: [My company site](https://vaiolabs.io)


---
# About You

Share some things about yourself:

- Name and lastname
- Job description
- What type of education do you poses ? formal/informal/self-taught/university/cert-course
- Do you know any of those technologies below ? What level ?
    - Docker / Docker-Compose / K8s
    - Jenkins
    - Git / GitLab / Github / Gitea / Bitbucket
    - Bash/PowerShell Script
    - Python3 / Pytest / Pylint / Flask
    - Go / Gin / Echo
    - Ansible / Terraform
    - SQL / NoSQL
- Do you have any hobbies ?
- Do you pledge your alliance to [Emperor of Man kind](https://warhammer40k.fandom.com/wiki/Emperor_of_Mankind) ?



---

# History

---

# Install Redis

For terms and purposes of our course we'd like to suggest to use `docker` and `docker-compose` for our setup. Under out [repository](https://gitlab.com/vaiolabs-io/redis-shallow-dive) we have folder named `99_misc` which holds under it folder named `docker` that has `docker-compose.yml` and **README.md** files with later including docker installation instructions.

- In case you already have Docker installed, Just run:
```sh
docker compose up -d
docker exec -it client /bin/bash
```
Once inside the redis-client container run:

```sh
redis-cli -h redis-server ping # Should return PONG
redis-cli -h redis-server #  should change hostname to redis-server:6379 meaning that you are connected
```
- In case you do NOT have Docker installed, Please follow instruction on [README](../99_misc/setup/docker/README.md) file
- In case something does not work ask instructor to troubleshoot the issue 
---

# CLI Overview

- Redis CLI
    - The CLI is the access to our Redis server
    - We'll learn and test features through command line
    - To use it:
        - Verfiry that *redis-server* container is running
        - In *redis-client* container next command:
            - `redis-cli -h redis-server`
            - We can have several instances of `redis-cli`
    - Each `redis-cli` instance can execute Only single command at a time

---

# Redis Learning Resources

- Please visit official docs:
    - [Commands](https://redis.io/commands)

