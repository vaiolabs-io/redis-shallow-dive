
---
# What Is Redis ?

- Definition: A key-value, in-memory, NoSQL database
    - Simple data formats:
        - No database schema
        - No complex data models
    - In Memory:
        - No hard drives
- Redis is FAST
- We UserRedis for:
    - Quick interactions
    - Simple information
        - Configuration options
        - Counters

- In memory data can be lost easily
  - Use Redis to ingest ephemeral data
  - Use a separate main database for long term storage
  - User REdis as an auxiliary system to improve performance


---

# Data Types

- Strings: 
    - Most basic type
    - Single key can store value with size of 512MB
- Lists:
    - Collections of things
    - Doubly linked
    - Up to four billion elements
    - Fantastic performance, even at large sizes
- Sets:
    - Collection, like lists, that do not allow repeat values
- Sorted sets:
    - Same as Sets yet sorted with efficient algorithm
- Hashes:
    - Map of key-value pairs
- Streams :
    - Allows you to define data that can be distributed to different clients

---
# Other Data Types

<img src="../99_misc/.img/we_dont_do_that_here.jpg" alt="not_deep" style="float:right;width:400px;">
There are also other data types:
- Bitmaps
- HyperLogLogs
- Geospatial indexes

Although there is use for them, diving into them will convert this course from **shallow** to **deep**, thus we don't do that here

---

# Setting Data

Let us dive into the Redis with commands, and we'll start with `SET`. The `SET` Command allows you to say a value in Redis. Here's the syntax :
```redis
SET Key Value [And Additional Parameters] 
```
- SET command is atomic meaning it can have only two outputs:
    - Success
    - Fail

---
# Practice

- Connect to redis-server
- setup key name with value of your name
- setup key lname with value of your last-name
- Setup key city_country with string value of city and country you live in

``` 
> SET name Silent
OK
> SET lname Mobius
OK
> SET city_country "Osaka, Japan"
OK
```

> `[!]` Note: in case KEY exist the value will be overwritten
---

# Getting Data

- `GET [key]`
- `GET` responses with the **string** value of the `[key]`
- In case where value does not exists you'll get value of `nil`
- If a space exists in value, then it can be contained with double quotes

```sh
> GET name
"Silent"
> GET lname
"Mobius"
> GET hero
(nil)

```
---

# Practice

---

# Strings With Redis

While `SET` and `GET` are pretty obvious and easy to use, there might be some question in regards to non string data type and how to operate them:

```sh
> SET counter 0
OK
> GET counter
"0"
> incr counter
(integer) 1
> dect counter
(integer) 0
```

As seen above we can increment and decrement with `incr/decr`. While in cases where we wish to use bigger numbers, we can use `incrby/decrby`

```sh
> SET counter 2
OK
> GET counter
"2"
> incrby counter 4
(integer) 6
> decrby counter 3
(integer) 3
> GET counter
"3"  # although it is an integer, when using GET we are receiving string.
```

To get the length of the string we use `STRLEN` command

```sh
> SET mykey "This is a long string"
OK
> STRLEN mykey
21
```
If there is a requirement of appending to existing key, we can use `APPEND` to specific `[key]`

```sh
> SET myname "Silent"
> APPEND myname "-Mobius" # once appended you receive the length of the string in integer
(integer) 13
> GET myname # in case you append to something that does not exists, key will be created and appended to it
"Silent-Mobius"
```
---

# Practice

- Create key "group-a" and save in it names as follows: (use space as delimiter)
    - Bruce
    - Diana
    - Clark
    - John
- Check the size of the key
- Append to "group-a" name "Wally"
- Get the whole "group-a"
- Check the length of the value of 'group-a'
- Show only first 11 characters in  "group-a"


```sh
> SET group-a "Bruce Diana Clark John"
> STRLEN group-a
> APPEND group-a " Wally"
> GETRANGE group-a 0 11
```