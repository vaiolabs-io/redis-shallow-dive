
---

# HashMap

---

# What Are Hashes ?
- A map where each key, maps to a single value
- Just like dict in Python
- Hashmaps are optimized for massive size
    - Up to for billion entries per map



---

# Read/Write Hashes

- `HGET` - will return the content of a key inside a hashmap
- `HSET` - will save a value inside hashmap
- Highly used case is track of the signed-in user
- `HMGET` - get more than one key
- `HMSET` - set more than one key


---

# Practice

- Create a HashMap with name of `menu` and include in it key-value pairs of navigation names and relevant path for them
    - Example:  contact: /some/short/path/to/contact.csv 
- Retrieve the value of the key that you have setup in `menu` hashmap
- Append to existing hashmap of menu additional key-value pairs
- Retrieve all of them
- Retrieve the amount of fields in whole hashmap called `menu`

---

# Exploring Hash Data

- HashMaps enables us to represent complex objects
- Objects can be presented with Key+ID+Property combination when data needs to be used by the applications but also later can be saved by RDBMS system
    - Converting existing Key+ID+Property object to SQL is easier then creating brand new object
- Use HGET and HSET to read/write from/to HashMaps


---

# Practice

