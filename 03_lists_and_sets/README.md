
---

# Lists

---

# Lists

- Simple and powerful collection of strings

<img src="../99_misc/.img/list.png" alt="list" style="float:right;width:180px;">

### Double linked lists

- Optimized data structure to interact with HEAD and TAIL of the list
- Direct reference to the first and last elements
- Each element knows about the previous on and next one
- One can add/remove elements from either end of the list
- There is a possibility to insert element in the middle of the list, yet it takes computation time which is usable but not recommended.

<img src="../99_misc/.img/list_actions.png" alt="list actions" style="float:right;width:180px;">

---

# Add/Remove elements to List

`Push` and `Pop` are the most common operators we'll use with List

- Pop: 
    - read one element from either end of the list
    - the element is popped (removed) from the list after that.
- Push
    - add one element into the list on either end

> `[+]` Note: Push/Pop only interact with either end of the list

### How to Push/Pop with redis ?

- Adding is done with:
    - `LPUSH`
    - `RPUSH`
- Getting is done with:
    - `LPOP`
    - `RPOP`

```sh
127.0.0.1:6379> lpush MYLIST "1st" "2nd" "last"
127.0.0.1:6379> lpop  MYLIST
"last"
```
Why did I get "last" if I push them on this order, first being the "1st" one?

### _The order of insertion matter_

Now, as you saw, the order of insertion and the place you insert them into matters quite a lot in Redis.
These three elements pushed into the head of the list, in order from left to right, will make the final element, you know, the, quote/unquote, "last" be the one that ends up being the first one in the resulted list. The list will look like this: final element, second element, and first element. Because when you insert the first one, is the first one into the list. But the second one, since you're inserting them on the left, from left to right, becomes the new first one. And the final element being the last one inserted into the head will become the new first one, leaving the first element to be the last one. It's a bit of a tongue twister, but you can see it here. If you were looking for the opposite result, you should have used RPUSH instead. Because with the same list of elements inserted in the same order, from left to right, but on the tail of the list, the result will look like this. You first insert the first element. 
---


> `[+]` Note: Popping an element modifies the list
# Practice

---

# Adding To Queue

---

# Removing From Queue

---

# Checking Queue: Who Is In It

---

# Adding To Queue In the Middle

---

# Checking Queue: Who Is At The End

---

# Practice

---

# Issues With Repeated Data

---

# Sets


---

# Sets Vs. Lists

---

# Adding To Queue Of Set

---

# Removing From Queue Of Set

---

# Practice

