# Docker Install

---

# The Easy Way To Install Docker

<img src="../99_misc/.img/lab.png" alt="lab" style="float:right;width:300px;">


```sh
curl -sL get.docker.com | sudo bash
```

---

# The Not So Easy Way To Install Docker


### Removing Old Versions

Due to dockers regular updates, it is suggested __NOT__ to install docker from your Linux distribution repositories. thus in case you have installed them, here is you can remove them.

In case of __RedHat__ based OS (Fedora/CentOS/RockyLinux)

```sh
sudo yum remove -y docker  docker-client docker-client-latest\
                    docker-common docker-latest docker-latest-logrotate\
                    docker-logrotate docker-engine
```


Or in case you are on __Debian__ based OS (Ubuntu/LinuxMint)

```sh
 sudo apt-get remove docker\
             docker-engine docker.io containerd runc
```


---

# Adding Docker Repository

In order to add docker to repository you'll need to access your root user account or on a contrary use `sudo` command. In my case I am going with root user.

We'll start by installing prerequisites on __RedHat__ based Linux distribution:

```sh
sudo yum install -y  yum-utils\
             device-mapper-persistent-data lvm2
```
And on __Debian__ based Linux distribution:

```sh
sudo apt-get update
sudo apt-get install apt-transport-https\
                             ca-certificates curl gnupg lsb-release
```


---
# Adding Docker Repository (cont.)

After adding initial tools, you can use __yum-utils__ included to tool __yum-config-manager__ to add docker-ce repository as follows:

```sh
 sudo yum-config-manager\
        --add-repo https://download.docker.com/linux/centos/docker-ce.repo
```
In case of __Debian__ based OS GnuPG certificate is needed, and only then the remote repository will work, so here are list of commands:

```sh
 curl -fsSL https://download.docker.com/linux/ubuntu/gpg\
         | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
```


And adding the repo to __sources.list.d__ folder

```sh
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg]\
            https://download.docker.com/linux/ubuntu  $(lsb_release -cs)\
            stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```


---

# Adding Docker Repository (cont.)

Adding your repo is not enough, you'll also need to install it, so just run:

```sh
sudo yum install -y docker-ce
```


Same goes for __Debian__ based OS

```sh
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
```


---
# Adding Docker Repository (cont.)

As any service on Linux box, you need to start and enable docker service.

```sh
sudo systemctl enable --now docker
```

In case you are planning not to use __root__ user on your box, then it is suggested to add your user to docker group

```sh
sudo usermod -aG docker ${USER}
```
> `[!]` Note: there is no difference between __RedHat/Debian__ distribution in this step

---
# Adding Docker Repository (cont.)

To verify  that it works it is suggested to test your docker.

```sh
 docker version
```
> `[!]` Note: there is no difference between __RedHat/Debian__ distribution in this step

