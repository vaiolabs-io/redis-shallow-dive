# Redis Shallow Dive

---

### Enough Redis to manage your data in **Re**mote **Di**ctionary **S**ervice

Redis course built around idea of learning enough to manage with Redis and also leave more space to grow and learn more

The course covers basic interaction with Redis with CLI and in future will cover Python api expansion with client 